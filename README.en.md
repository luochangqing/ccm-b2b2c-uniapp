# ccm-b2b2c-uniapp多商户商城

#### 介绍
创创猫B2B2C多商户商城，是一款完善且经过线上验证的Java电商系统。  
商城前端使用uni-app开发, 可打包部署到微信小程序, APP, H5，系统后台则是用java springboot开发。   
本项目是用户端的前端源码, 在项目详情里你也能获取到商家端/平台端/Java后台的源码。  

#### 软件架构

前端使用uni-app开发, uni-app 是一个使用 Vue.js 开发所有前端应用的框架。  
开发者编写一套代码，可发布到iOS、Android、H5、以及各种小程序（微信/支付宝/百度/头条/QQ/钉钉）等多个平台。  

#### 安装教程

1.  在https://www.dcloud.io/hbuilderx.html 下载Hbuilder X  
2.  在Hbuilder X中导入该项目  
3.  使用Hbuilder X运行到浏览器Chrome(也可运行到微信小程序开发工具)  

#### 项目演示

1.  关注公众号即可获取演示地址及账号密码  
![Image text](https://ccmao-store.oss-cn-shenzhen.aliyuncs.com/ccm_sp.jpg)  

#### 后台源码

1.  添加微信免费获取商家端、平台端、JAVA后台项目源码  
![Image text](https://ccmao-store.oss-cn-shenzhen.aliyuncs.com/personal_wechat.png)  

#### 功能介绍

商家管理，商家入驻申请，商家审核  
商品管理，单规格、多规格商品管理，品牌、分类管理、商品评价、商品组  
订单管理，订单支付、发货、取消、售后等  
会员管理，会员列表、会员等级等  
奖金管理，会员提现、商家提现、奖金指出、团队业绩  
内容管理，官方资讯、活动通知、素材管理、常见问题  
营销推广，首页轮播、广告管理、优惠券、团购秒杀、拼团管理  
应用设置，基础设置、微信支付、支付宝支付、多媒体存储、短信设置  
分润设置，推广分润、销售分润、团队业绩分润、培训分润  
统计分析，商品销量统计、会员增长统计、订单销量统计  


#### 项目截图

![Image text](https://ccmao-b2c.oss-cn-shenzhen.aliyuncs.com/b2b2c-app.png)  
![Image text](https://ccmao-b2c.oss-cn-shenzhen.aliyuncs.com/b2b2c-pc-1.png)  
![Image text](https://ccmao-b2c.oss-cn-shenzhen.aliyuncs.com/b2b2c-pc-2.png)
![Image text](https://ccmao-b2c.oss-cn-shenzhen.aliyuncs.com/b2b2c-pc-3.png)